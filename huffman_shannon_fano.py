from huffman import encode_huffman, decode_huffman
from shannon_fano import encode_shannon_fano, decode_shannon_fano
import os
import hashlib
import sys

def main():
    input_file = 'test.txt'

    # check if filename is given
    if len(sys.argv) > 1:
        input_file = sys.argv[1]

    # check if file exists
    if not os.path.isfile(input_file):
        print("ERROR: File %s not found." % input_file)
        print("Exiting...")
        return

    # set filenames
    encoded_huff_file = input_file + '.huff'
    encoded_fs_file = input_file + '.fs'

    # encode file using huffman
    print('Huffman encoding started...')
    encode_huffman(input_file)
    print('Huffman encoding finished!')

    # decode it
    print('Huffman decoding started...')
    decode_huffman(encoded_huff_file)
    print('Huffman decoding finished!')
    print()

    # encode file using shannon-fano
    print('Shannon-Fano encoding started...')
    encode_shannon_fano(input_file)
    print('Shannon-Fano encoding finished!')

    # decode it
    print('Shannon-Fano decoding started...')
    decode_shannon_fano(encoded_fs_file)
    print('Shannon-Fano decoding finished!')

    # get file sizes
    input_file_size = os.path.getsize(input_file)
    huffman_file_size = os.path.getsize(encoded_huff_file)
    fs_file_size = os.path.getsize(encoded_fs_file)

    # calculate compression percent
    huffman_compression_percent = (input_file_size - huffman_file_size)/input_file_size*100
    fs_compression_percent = (input_file_size - fs_file_size)/input_file_size*100

    # print results
    print()
    print("Uncompressed file size (in bytes):         {0}".format(input_file_size))
    print("Huffman encoded file size (in bytes):      {0} ({1:.2f}% compression)".format(huffman_file_size, huffman_compression_percent))
    print("Fano-Shannon encoded file size (in bytes): {0} ({1:.2f}% compression)".format(fs_file_size, fs_compression_percent))

    # calculate hashes and print message
    input_file_hash = hashlib.md5( open(input_file, 'rb').read() ).hexdigest()
    huffman_file_hash = hashlib.md5(open(encoded_huff_file + '.decoded', 'rb').read()).hexdigest()
    fs_file_hash = \
        hashlib.md5(
            open(encoded_fs_file + '.decoded', 'rb').read()).hexdigest()
    print()
    print("Input file md5 hash:                {0}".format(input_file_hash))
    print("Huffman decoded file md5 hash:      {0}".format(huffman_file_hash))
    print("Shannon-Fano decoded file md5 hash: {0}".format(fs_file_hash))

    # check if hashes of decoded files match
    if input_file_hash == huffman_file_hash == fs_file_hash:
        print("All hashes match!")
    else:
        print("Hashes are different. Error in encoding/decoding.")

if __name__ == '__main__':
    main()
