from huffman import freq_string, create_node_list, Node, add_codes_to_nodes, get_labels_from_tree, encode, decode
import operator
import sys
import pickle



def fs_tree(node_list):
    if type(node_list) is not list:
        print("NOT A LIST!")
        return

    i = 1
    length = len(node_list)
    minimum_diff = 999999999
    minimum_index = -1
    while i < length:
        # get the left half
        left = node_list[0:i]
        # calculate left weight
        left_weight = node_list_weight(left)

        # get the right half
        right = node_list[i:]
        # calculate right weight
        right_weight = node_list_weight(right)

        # find difference
        difference = abs(right_weight - left_weight)

        # compare to minimum_diff
        if difference <= minimum_diff:
            minimum_diff = difference
            minimum_index = i
        # if larger then break!
        else:
            break
        i+=1

    # get left and right
    left = node_list[0:minimum_index]
    right = node_list[minimum_index:]

    # if one node set it as child
    if len(left) == 1:
        left = left[0]

    if len(right) == 1:
        right = right[0]

    # create parent node
    parent = create_parent_fs(left, right)

    # and run fs_tree for left and right
    if type(left) is list:
        parent.left = fs_tree(parent.left)
    if type(right) is list:
        parent.right = fs_tree(parent.right)

    # return parent node
    # if node is root
    if parent.parent is None:
        # add codes
        add_codes_to_nodes(parent)
    # and return node
    return parent



def create_parent_fs(left, right):
    weight = 0

    # get the weight of the left child
    if type(left) == Node:
        weight += left.weight
    else:
        weight += node_list_weight(left)

    # get the weight of the right child
    if type(right) == Node:
        weight += right.weight
    else:
        weight += node_list_weight(right)
    # create a parent
    parent = Node(weight)

    # set the children
    parent.left = left
    parent.right = right

    if type(left) is Node:
        left.parent = parent
    if type(right) is Node:
        right.parent = parent

    return parent


# calculate the weight of a list of nodes
def node_list_weight(node_list):
    total_weight = 0
    for node in node_list:
        total_weight += node.weight
    return total_weight


def encode_shannon_fano(input_file):
    # open file and read data
    file = open(input_file, 'rb')
    data = bytearray(file.read())
    extension = '.fs'
    # create frequency dictionary
    freq = freq_string(data)
    # create the tree
    node_list = create_node_list(freq)
    node_list.sort(key=operator.attrgetter('weight'), reverse=True)

    root = fs_tree(node_list)
    # get the codes
    codes = {}
    get_labels_from_tree(root, codes)

    # encode and write file
    encode(input_file, '.fs', codes)
    # dump freqs
    pickle.dump(freq, open(input_file + extension + '.freq', 'wb'))


def decode_shannon_fano(input_file):
    extension = '.fs'
    #load symbols
    freq = pickle.load(open(input_file + '.freq', 'rb'))
    # recreate tree
    node_list = create_node_list(freq)
    # sort the node list
    node_list.sort(key=operator.attrgetter('weight'), reverse=True)
    # get the root
    root = fs_tree(node_list)

    decode(input_file, root)
