from collections import defaultdict
from bitstring import BitStream
import operator
import pickle


# given a string
# find each character's frequency
# and return it as a dictionary
def freq_string(string):
    freq = defaultdict(int)
    for char in string:
        freq[char] += 1
    return freq


class Node:
    def __init__(self, weight, value=None):
        self.weight = weight
        self.value = value
        self.code = ''

        self.parent = None
        self.left = None
        self.right = None


# given two nodes
# create a parent and assign them as children
def create_parent(a, b):
    left = None
    right = None

    if a.weight > b.weight:
        right = b
        left = a
    else:
        right = a
        left = b
    # parent weight is the sum of his children
    parent = Node(a.weight + b.weight)

    parent.left = left
    parent.right = right

    left.parent = parent
    right.parent = parent

    return parent


def create_node_list(freq_hash):
    # create an empty list
    node_list = []
    # for each key value pair in the frequency dictionary
    for k, v in freq_hash.items():
        # create a new node
        node = Node(v, k)
        # and append it to the list
        node_list.append(node)

    return node_list


def create_huffman_tree(freq_hash):
    # create nodes for each entry in freq hash
    node_list = create_node_list(freq_hash)

    # sort them based on weight
    node_list.sort(key=operator.attrgetter('weight'))

    # while the list contains two or more nodes
    # (when we have only one node it is the root of our tree)
    while len(node_list) > 1:
        # get the two nodes withe minimum weight
        node_a = node_list.pop(0)
        node_b = node_list.pop(0)
        # create a parent for them
        parent = create_parent(node_a, node_b)
        # add the parent to the list
        node_list.append(parent)
        # and sort the list based on the weight of the nodes
        node_list.sort(key=operator.attrgetter('weight'))

    # get the (only) element from the list
    root = node_list.pop()
    # add codes to them
    add_codes_to_nodes(root)

    return root


# assign codes to the nodes using DFS
def add_codes_to_nodes(node):
    # if the node has a left child
    if node.left is not None:
        # add 0 to the child's code
        node.left.code = node.code + '0'
        # and then call self for the left child
        add_codes_to_nodes(node.left)

    # if the node has a right child
    if node.right is not None:
        # add 1 to the child's code
        node.right.code = node.code + '1'
        # and then call self for the right child
        add_codes_to_nodes(node.right)


# given a node and a dictionary to fill up
# get the codes from the nodes using DFS
def get_labels_from_tree(root, codes):
    # if the node is a leaf (hash no children)
    if (root.left is None and root.right is None):
        # add it's code to the dictionary
        codes[root.value] = root.code

    # if it has a left child call self for it
    if root.left is not None:
        get_labels_from_tree(root.left, codes)

    # if it has a right child call self for it
    if root.right is not None:
        get_labels_from_tree(root.right, codes)


def encode_huffman(input_file):
    # open file (in binary mode)
    file = open(input_file, 'rb')
    # read it's data
    data = bytearray(file.read())
    # create a frequency dictionary
    freq = freq_string(data)
    # create the huffman tree based on the frequency dictionary
    root = create_huffman_tree(freq)
    # create an empty dictionary
    codes = {}
    # and fill it up
    get_labels_from_tree(root, codes)
    # encode the given file
    encode(input_file, '.huff', codes)
    # and save the frequency dictionary to a file
    pickle.dump(freq, open(input_file + '.huff' + '.freq', 'wb'))


# the encoding function (used for both algorithms)
def encode(input_file, extension, codes):
    # open the given file
    file = open(input_file, 'rb')
    # get its contents in array of bytes
    data = bytearray(file.read())

    # open the output file
    out = open(input_file + extension, 'wb')

    output_bitstream = BitStream()

    i = 0
    length = len(data)
    output_string = '0b'
    while i < length:
        code1 = codes[data[i]]
        if i + 50 < length:
            code1 += \
                codes[data[i+1]] + codes[data[i+2]] + codes[data[i+3]] + \
                codes[data[i+4]] + codes[data[i+5]] + codes[data[i+6]] + \
                codes[data[i+7]] + codes[data[i+8]] + codes[data[i+9]] + \
                codes[data[i+10]] + codes[data[i+11]] + codes[data[i+12]] + \
                codes[data[i+13]] + codes[data[i+14]] + codes[data[i+15]] + \
                codes[data[i+16]] + codes[data[i+17]] + codes[data[i+18]] + \
                codes[data[i+19]] + codes[data[i+20]] + codes[data[i+21]] + \
                codes[data[i+22]] + codes[data[i+23]] + codes[data[i+24]] + \
                codes[data[i+25]] + codes[data[i+26]] + codes[data[i+27]] + \
                codes[data[i+28]] + codes[data[i+29]] + codes[data[i+30]] + \
                codes[data[i+31]] + codes[data[i+32]] + codes[data[i+33]] + \
                codes[data[i+34]] + codes[data[i+35]] + codes[data[i+36]] + \
                codes[data[i+37]] + codes[data[i+38]] + codes[data[i+39]] + \
                codes[data[i+40]] + codes[data[i+41]] + codes[data[i+42]] + \
                codes[data[i+43]] + codes[data[i+44]] + codes[data[i+45]] + \
                codes[data[i+46]] + codes[data[i+47]] + codes[data[i+48]] + \
                codes[data[i+49]] + codes[data[i+50]]
            i += 51
        else:
            i +=1

        output_string += code1

    output_bitstream.append(output_string)
    # find the padding bits to append to the end of the file
    padding = len(output_bitstream) % 8

    # create padding and append it (if necessary)
    if padding > 0:
        padding = 8 - padding
        padding_str = ''
        padding_str = '0' * padding
        output_bitstream.append('0b' + padding_str)

    # add the number of the padding bits to the beginning of the file
    output_bitstream.prepend('0x0' + str(padding))

    # write to file
    output_bitstream.tofile(out)
    # and close the file
    out.close()


def decode_huffman(encoded_file):
    # load frequency dictionary from file
    freq = pickle.load(open(encoded_file + '.freq', 'rb'))
    # recreate tree
    root = create_huffman_tree(freq)
    # decode the file using the tree
    decode(encoded_file, root)


def decode(encoded_file, root):
    # load file on BitStream
    encoded = BitStream(bytes=open(encoded_file, 'rb').read())
    decoded = BitStream()
    # get number of padding bits
    padding = encoded.bytes[0]
    # and delete it
    del encoded[0:8]
    # remove padding bytes from the end of BitStream
    if padding > 0:
        del encoded[-padding:]

    output_string = '0x'
    node = root
    for bit in encoded:
        # traverse the tree
        if bit:
            node = node.right
        else:
            node = node.left

        # when node is leaf
        if node.left is None and node.right is None:
            # append it to the output
            output_string += "{0:#0{1}x}".format(int(node.value), 4)[2:]
            # and set current node to root
            node = root

    # write decoded file
    decoded.append(output_string)
    decoded.tofile(open(encoded_file + '.decoded', 'wb'))
